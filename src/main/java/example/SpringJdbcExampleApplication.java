package example;

import example.homework12.dao.BookDAO;
import example.homework12.dao.UserDAO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class SpringJdbcExampleApplication implements CommandLineRunner {
    private UserDAO userDAOBean;
    private BookDAO bookDAOBean;

    //иньекция ,бинов классов через конструктор
    /**
     * 3 пункт задания:
     * Создать класс-конфигурацию, где можно создать Bean класса UserDao.
     * Экземпляр класса UserDao должен быть создан каждый раз новый, при обращении к нему через bean.
     * Протестировать в главном классе, что у вас происходит DI вашего бина и вы можете выполнять все CRUD операции с таблицей клиентов.
     */
    public SpringJdbcExampleApplication(UserDAO userDAOBean, BookDAO bookDAOBean) {
        this.userDAOBean = userDAOBean;
        this.bookDAOBean = bookDAOBean;
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringJdbcExampleApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        //Устанавливаем класс - конфигурацию
        //ApplicationContext ctx = new AnnotationConfigApplicationContext(MyDataBaseContext.class);
        //log.info(bookDAO.getBookById(1).toString());
        //bookDAO.insertBook(new Book(2, "Незнайка на Луне", "Н.Н. Носов", LocalDate.now()));
        log.info(bookDAOBean.getAllFromBooks().toString());
        log.info(userDAOBean.getAllFromUsers().toString());
        log.info(userDAOBean.getBooksFromUser("89090123657").toString());

    }
}
