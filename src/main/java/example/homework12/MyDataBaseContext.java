package example.homework12;

import example.homework12.dao.BookDAO;
import example.homework12.dao.UserDAO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Configuration
@ComponentScan
public class MyDataBaseContext {
    private static final String DB_HOST = "localhost";
    private static final String DB_PORT = "5555";
    private static final String DB_NAME = "homework12";
    private static final String DB_USER = "user";
    private static final String DB_PASSWORD = "12345";

    @Bean
    public DataSource dataSource() {
        return new DriverManagerDataSource("jdbc:postgresql://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME,
                DB_USER,
                DB_PASSWORD);
    }

    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }

    @Bean
    public JdbcTemplate jdbcTemplate1() {
        return new JdbcTemplate(dataSource());
    }
}
