CREATE TABLE clients (
    id serial primary key,
    last_name varchar not null ,
    first_name varchar not null ,
    date_of_birth date not null ,
    phone_number varchar not null ,
    books_id INTEGER ARRAY
);

CREATE TABLE books (
    id serial primary key,
    title varchar(30) not null ,
    author varchar(30) not null ,
    date_added timestamp not null
);

INSERT INTO books (title, author, date_added) VALUES (
    'Мастер и Маргарита', 'М.А. Булгаков', '1995-6-15'
        );

INSERT INTO clients (last_name, first_name, date_of_birth, phone_number, books_id) VALUES (
    'Громов', 'Андрей', '1994-9-20','89090123657', '{1, 2}'
                                                                                          );

SELECT * FROM clients;