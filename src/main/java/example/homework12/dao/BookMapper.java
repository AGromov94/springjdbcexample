package example.homework12.dao;

import example.homework12.model.Book;
import org.springframework.jdbc.core.RowMapper;

import javax.swing.tree.TreePath;
import java.sql.ResultSet;
import java.sql.SQLException;

import static net.sf.jsqlparser.parser.feature.Feature.set;
// реализация собственного маппера, сейчас работает BeanPropertyRowMapper из спринга
//public class BookMapper implements RowMapper<Book> {
//    @Override
//    public Book mapRow(ResultSet, int i) throws SQLException {
//        Book book = new Book(resultSet.getInt("id"), resultSet.getString("title"), resultSet.getString("author"), resultSet.getDate("date_added"));
//        return book;
//    }
//}
