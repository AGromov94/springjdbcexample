package example.homework12.dao;

import example.homework12.model.Book;
import example.homework12.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class UserDAO {
    private final JdbcTemplate jdbcTemplate;

    public UserDAO(JdbcTemplate userJdbcTemplate) {
        this.jdbcTemplate = userJdbcTemplate;
    }

    public List<User> getAllFromUsers() {
        return jdbcTemplate.query("SELECT * FROM clients", new BeanPropertyRowMapper<>(User.class));
    }

    public User getUserById(int id) {
        return jdbcTemplate.query("SELECT * FROM clients WHERE id=?", new Object[]{id}, new BeanPropertyRowMapper<>(User.class))
                .stream().findAny().orElse(null);
    }

    public void insertUser(User user) {
        jdbcTemplate.update("INSERT INTO clients VALUES(?, ?, ?, ?, ?, ?)", user.getId(), user.getLastName(), user.getFirstName(), user.getDateOfBirth(),
                user.getPhoneNumber(), user.getBooksId());
    }

    public void updateUserById(int id, User updatedUser) {
        jdbcTemplate.update("UPDATE clients SET last_name=?, first_name=?, date_of_birth=?, phone_number=?, books_id=? WHERE id=?", updatedUser.getLastName(), updatedUser.getFirstName(),
                updatedUser.getDateOfBirth(), updatedUser.getPhoneNumber(), updatedUser.getBooksId());
    }

    public void deleteUserById(int id) {
        jdbcTemplate.update("DELETE FROM clients WHERE id=?", id);
    }


    /**
     * Метод в классе UserDao, который принимает телефон,
     * достает из UserDAO список книг данного человека и конвертирует их в объект типа Book через класс BookDao.
     * На выходе иметь List<Book>.
     */
    public List<Book> getBooksFromUser(String phone) throws NoSuchFieldException, SQLException {
        List<Book> list = new ArrayList<>();
        Array e = jdbcTemplate.queryForObject("SELECT books_id FROM clients WHERE phone_number=?", java.sql.Array.class, phone);
        Integer[] ids = (Integer[]) e.getArray();
        for (int id : ids) {
            BookDAO bookDAO = new BookDAO(jdbcTemplate);
            list.add(bookDAO.getBookById(id));
        }
        return list;
    }
}
