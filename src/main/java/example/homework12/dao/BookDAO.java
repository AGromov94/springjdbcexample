package example.homework12.dao;

import example.homework12.model.Book;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class BookDAO {
    private final JdbcTemplate jdbcTemplate;

    public BookDAO(JdbcTemplate bookJdbcTemplate) {
        this.jdbcTemplate = bookJdbcTemplate;
    }

    public List<Book> getAllFromBooks() {
        return jdbcTemplate.query("SELECT * FROM books", new BeanPropertyRowMapper<>(Book.class));
    }

    public Book getBookById(int id) {
        return jdbcTemplate.query("SELECT * FROM books WHERE id=?", new Object[]{id}, new BeanPropertyRowMapper<>(Book.class))
                .stream().findAny().orElse(null);
    }

    public void insertBook(Book book) {
        jdbcTemplate.update("INSERT INTO books VALUES(?, ?, ?, ?)", book.getId(), book.getTitle(), book.getAuthor(), book.getDateAdded());
    }

    public void updateBookById(int id, Book updatedBook) {
        jdbcTemplate.update("UPDATE books SET title=?, author=?, date_added=? WHERE id=?", updatedBook.getTitle(), updatedBook.getAuthor(),
                updatedBook.getDateAdded());
    }

    public void deleteBookById(int id) {
        jdbcTemplate.update("DELETE FROM books WHERE id=?", id);
    }
}
