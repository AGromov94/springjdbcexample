package example.homework12.model;

import lombok.*;

import java.sql.Array;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
public class User {
    private Integer id;
    private String lastName;
    private String firstName;
    private LocalDate dateOfBirth;
    private String phoneNumber;
    private Array[] booksId;
}
