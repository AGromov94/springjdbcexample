package example.homework12.model;

import lombok.*;

import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
public class Book {
    private Integer id;
    private String title;
    private String author;
    private LocalDate dateAdded;
}
